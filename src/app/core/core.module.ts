import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import {AppRouterModule} from '../app.route';
import {SystemsService} from '../systems/systems.service';
import {AuthenticationService} from '../auth/authentication.service';
import {SharedModule} from '../shared/shared.module';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [HomeComponent, NavigationComponent],
  imports: [
    SharedModule,
    AppRouterModule,
    CommonModule
  ],
  exports: [
    AppRouterModule,
    NavigationComponent,
  ],
  providers: [
    SystemsService,
    AuthenticationService,
  ]
})
export class CoreModule { }
