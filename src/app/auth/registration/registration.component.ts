import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RegistrationDc} from '../../contracts/auth.contracts';

@Component({
  selector: 'app-signup',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  private return = '/';
  hide = true;
  errorMessage = '';

  registrationForm: FormGroup;

  constructor(private authService: AuthenticationService, private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required],
      isManager: [false, Validators.required]
    });

  }

  onSignup() {
    const registrationData: RegistrationDc = {
      username: this.registrationForm.get('username').value,
      email: this.registrationForm.get('email').value,
      password1: this.registrationForm.get('password').value,
      password2: this.registrationForm.get('repeatPassword').value,
      isManager: this.registrationForm.get('isManager').value,
    };
    console.log(registrationData);
    this.authService.doRegistration(registrationData).then(resolve => {

      this.router.navigateByUrl(this.return);
    }, reason => {
      this.errorMessage = 'Registration failed.';
      if (reason.username) {
        this.errorMessage = this.errorMessage + '\n ' + reason.username;
      }

      if (reason.email) {
        this.errorMessage += '\n ' + reason.email;
      }
    });
  }

  isInvalid(): boolean {
    return this.registrationForm.invalid;
  }

}
