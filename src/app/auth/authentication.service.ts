import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../contracts/base.contracts';
import {AuthenticationApiUrl, LoginDc, RegistrationDc, TokenDc} from '../contracts/auth.contracts';
import {WebsocketService} from '../shared/websocket.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService extends BaseService {

  constructor(http: HttpClient, private wsService: WebsocketService) {
    super(http);
  }

  get token(): string {
    try {

      const user = JSON.parse(sessionStorage.getItem('currentUser')) as { key: string };
      if (user && user.key !== '') {
        return user.key;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  get isAuthenticated(): boolean {
    return this.token != null;
  }

  doLogin(userData: LoginDc) {
    return new Promise((resolve, reject) => {
      this.http.post(AuthenticationApiUrl.login, userData).toPromise()
        .then(res => {
            const result = res as TokenDc;
            sessionStorage.setItem('currentUser', JSON.stringify(result));
            resolve();
          },
          msg => {
            reject(msg);
          }
        )
        .catch(
          err => {
            reject(err);
          });
    });
  }

  doLogout() {
    sessionStorage.removeItem('currentUser');
    this.http.post(AuthenticationApiUrl.logout, {}).toPromise().then();
  }

  doRegistration(data: RegistrationDc) {
    return new Promise((resolve, reject) => {
      this.doPost(AuthenticationApiUrl.register, data).toPromise()
        .then(res => {
            const result = res as TokenDc;
            sessionStorage.setItem('currentUser', JSON.stringify(result));

            resolve();
          },
          msg => {
            console.log(msg);
            reject(msg.error);
          }
        )
        .catch(
          err => {
            reject(err);
          });
    });
  }
}
