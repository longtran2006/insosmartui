import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../authentication.service';
import {FormControl, NgForm, Validators} from '@angular/forms';
import {LoginDc} from '../../contracts/auth.contracts';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  return = '';
  hide = true;
  errorMessage = '';

  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);
  userName = new FormControl('', [Validators.required]);

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {

    // Get the query params
    this.route.queryParams
      .subscribe(params => this.return = params.return || '/');
  }

  onLogin() {
    const userData: LoginDc = {
      username: this.userName.value,
      // email: this.email.value,
      password: this.password.value,
    };

    console.log(userData);
    this.authenticationService.doLogin(userData).then(resolve => {
      if (this.authenticationService.isAuthenticated) {
        this.errorMessage = '';
        this.router.navigateByUrl(this.return);
      }
    }).catch(reject => {
      this.errorMessage = 'Username or password is not correct';
    });
  }

  getErrorMessage() {
    return this.errorMessage;
  }

  onRediectTo(url: string) {
    this.router.navigateByUrl(url);
  }
}
