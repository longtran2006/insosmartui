import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public authenticationService: AuthenticationService, private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //
    if (this.authenticationService.isAuthenticated) {
      request = request.clone({
        headers: request.headers.set('Authorization', 'Token ' + this.authenticationService.token)
        /*
        setHeaders: {
          Authentication: 'Token ' + this.authenticationService.token,
        }
        */
      });
    }

    return next.handle(request).pipe(
      catchError(error => {

        if (error.status === 401 || error.status === 419) {
          // logout users, redirect to login page
          this.authenticationService.doLogout();
          this.router.navigate(['/login']);
          return Observable.throw(error);

        }

        if (error.status === 400) {
          throw error;
        }
        /*
        if (error.status === 419) {

          return this.authenticationService.refreshToken().flatMap(t => {
            const authReq = req.clone({ headers: req.headers.set('Authorization', 'Token ' + t) });
            return next.handle(authReq);
          });

        }*/

        // return all others errors
        return Observable.throw(error);

      })) as any;
  }
}
