/**
 * Created by Nong on 8/5/2017.
 */
import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {HomeComponent} from './core/home/home.component';
import {GuardService} from './auth/guard.service';

const appRoutes: Routes = [
  {path: '', component: HomeComponent,  canActivate: [GuardService]},
  {path: 'systems', loadChildren: './systems/systems.module#SystemsModule'},
  // otherwise redirect to home
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRouterModule {

}
