import {BaseApi} from './base.contracts';

export interface LoginDc {
  username: string;
  password: string;
}

export interface RegistrationDc {
  username: string;
  email: string;
  password1: string;
  password2: string;
  isManager: boolean;
}

export interface TokenDc {
  key: string;
}

export class AuthenticationApiUrl {
  public static login = BaseApi + '/auth/login/';
  public static logout = BaseApi + '/auth/logout/';
  public static register = BaseApi + '/auth/registration/';
}
