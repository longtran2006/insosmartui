import {BaseApi, BaseDc, Pagination} from './base.contracts';

export interface SystemDc extends BaseDc {
  name: string;
  description: string;
  system_type: string;
  trainned_date: Date;
  created_date: Date;
  modified_date: Date;
  is_trained: boolean;
  is_imported: boolean;
}

export interface SystemSaveRequestDc {
  name: string;
  description: string;
  system_type: string;
}

export interface MeasurementPredictingDataDc {
  first_comp: number;
  second_comp: number;
}

export interface MeasurementIdentityDc {
  first_comp: number;
  second_comp: number;
  label: number;
}

export interface ISystemApiService {
  getAllSystems(page: Pagination): SystemDc[];

  getSystems(): SystemDc[];

  getDetailSystem(id: number): SystemDc | Promise<SystemDc>;

  createSystem(request: SystemSaveRequestDc): any;

  deleteSystem(id: number): Promise<any>;

  updateSystem(id: number, request: SystemSaveRequestDc): any;

  trainDataSystem(id: number, data: any): Promise<any>;

  importDataSystem(id: number): Promise<any>;

  getClusterData(id: number): Promise<any>;

  getPredictedData(id: number, data: MeasurementPredictingDataDc): Promise<any>;

  getMeasuredData(id: number, data: MeasurementIdentityDc): Promise<any>;

  updateMeasuredData(id: number, data: any): Promise<any>;

  getRegressionData(id: number): Promise<any>;

  getSomData(id: number): Promise<any>;
}

export class SystemApiUrl {
  public static getAllSystems = BaseApi + '/systems/getAll';
  public static createSystem = BaseApi + '/systems/create';
  public static detailSystem = BaseApi + '/systems/detail/';
  public static editSystem = BaseApi + '/systems/edit/';
  public static deleteSystem = BaseApi + '/systems/delete/';
  public static trainDataSystem = BaseApi + '/systems/trainData/';
  public static importDataSystem = BaseApi + '/systems/importData/';
  public static getClusterData = BaseApi + '/systems/getClusterData/';
  public static getPredictedData = BaseApi + '/systems/getPredictedData/';
  public static getMeasuredData = BaseApi + '/systems/getMeasurementData/';
  public static updateMeasuredData = BaseApi + '/systems/updateMeasurementData/';
  public static getRegressionData = BaseApi + '/systems/getRegressionData/';
  public static getSomClusterData = BaseApi + '/systems/getSomClusterData/';
}

