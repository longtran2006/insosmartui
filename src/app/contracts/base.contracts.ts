import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

export const BaseApi = 'http://localhost:8990/apis';
export const SocketEndpoint = 'ws://localhost:8990/ws';

export interface Pagination {
  pageNumber: number;
  pageSize: number;
}

export interface BaseDc {
  id?: number;
}

export interface PageDc {
  page: number;
  size: number;
}

export interface RequestDc {
  topic: string;
  data: any;
}

export interface MessageDc {
  topic: string;
  data: any;
}

export class BaseService {

  constructor(http: HttpClient) {
    this.http = http;
  }

  protected http: HttpClient;

  private static extractData(res: Response) {
    // return res.json();
    return res;
  }

  public doPost(url: string, data: any): any {
    return this.http.post(url, JSON.stringify(data))
      .pipe(
        map(BaseService.extractData));
    // catchError(this.handleError));
  }

  public doPut(url: string, data: any): any {
    return this.http.put(url, JSON.stringify(data))
      .pipe(
        map(BaseService.extractData));
    // catchError(this.handleError));
  }

  public doDelete(url: string): any {
    return this.http.delete(url);
  }

  public doGet(url: string): any {
    return this.http.get(url)
      .pipe(
        map(BaseService.extractData));
    // catchError(this.handleError));
  }

  public doGetWithPagination(url: string, page: Pagination): any {

    const newUrl = url + '?page=' + page.pageNumber + '&size=' + page.pageSize;
    return this.http.get(newUrl)
      .pipe(map(BaseService.extractData));
  }

  /*
  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.topic ? error.topic : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }*/
}
