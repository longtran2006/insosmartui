import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Data, Layout} from 'plotly.js';

@Component({
  selector: 'app-line-plot',
  templateUrl: './line-plot.component.html',
  styleUrls: ['./line-plot.component.css']
})
export class LinePlotComponent implements OnInit {

  @Input() public graph = {
    data: [
      {
        x: ['2016-10-13 09:15:23.456', '2016-10-13 09:15:24.456', '2016-10-13 09:15:25.456',
          '2016-10-13 09:15:26.456', '2016-10-13 09:15:27.456', '2016-10-13 09:15:28.456'],
        y: [1, 2, 3, 4, 5, 6],
        type: 'scatter', mode: 'lines+points', marker: {color: 'red'}
      },
      {
        x: ['2016-10-13 09:15:23.456', '2016-10-13 09:15:24.456', '2016-10-13 09:15:25.456',
          '2016-10-13 09:15:26.456', '2016-10-13 09:15:27.456', '2016-10-13 09:15:28.456'],
        y: [3, 2, 7, 4, 9, 6],
        type: 'scatter', mode: 'lines+points', marker: {color: 'red'}
      },
    ] as unknown as Data[],
    layout: {width: 800, height: 600, title: 'A Fancy Plot'} as Layout
  };

  @Output() selectedPoint = new EventEmitter(true);

  constructor() {
  }

  ngOnInit() {
  }


  onClick(event: any) {
    console.log(event);
  }

  onSelect(event: any) {
    if (event.points) {
      const selectedPoint = event.points[0];
      if (selectedPoint) {
        const data = {
          x: selectedPoint.x,
          y: selectedPoint.y,
          color: selectedPoint['marker.color']
        };
        this.selectedPoint.emit(data);
      }
    }
  }
}
