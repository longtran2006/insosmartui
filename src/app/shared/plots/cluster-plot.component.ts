import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Data, Layout} from 'plotly.js';

@Component({
  selector: 'app-cluster-plot',
  templateUrl: './cluster-plot.component.html',
  styleUrls: ['./cluster-plot.component.css']
})
export class ClusterPlotComponent implements OnInit {

  @Input() public graph = {
    data: [
      {x: [1, 2, 3], y: [2, 6, 3], type: 'scatter', mode: 'lines+points', marker: {color: 'red'}},
      {x: [1, 2, 3], y: [2, 5, 3], type: 'bar'},
    ] as Data[],
    layout: {width: 600, height: 600, title: 'A Fancy Plot'} as Layout
  };

  @Output() selectedPoint = new EventEmitter(true);

  constructor() {
  }

  ngOnInit() {
  }


  onClick(event: any) {
    console.log(event);
  }

  onSelect(event: any) {
    if (event.points) {
      const selectedPoint = event.points[0];
      if (selectedPoint) {
        const data = {
          x: selectedPoint.x,
          y: selectedPoint.y,
          color: selectedPoint['marker.color']
        };
        this.selectedPoint.emit(data);
      }
    }
  }
}
