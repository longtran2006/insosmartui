export interface CommandAction {
  actionName: string;
  action: () => void;
  level: string;
  enabled: boolean;
  visible: boolean;
}

export interface Column {
  key: string;
  name: string;
}
