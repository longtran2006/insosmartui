import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WebsocketService} from './websocket.service';
import {InsoHeaderComponent} from './inso-layout/inso-header.component';
import {InsoDetailLayoutComponent} from './inso-layout/inso-detail-layout.component';
import {InsoListLayoutComponent} from './inso-layout/inso-list-layout.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatTableModule
} from '@angular/material';
import {CreateSystemDialogComponent} from './dialogs/create-system-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ClusterPlotComponent} from './plots/cluster-plot.component';
import {PlotlyModule} from 'angular-plotly.js';
import {LinePlotComponent} from './plots/line-plot.component';


@NgModule({
  declarations: [
    InsoHeaderComponent,
    InsoDetailLayoutComponent,
    InsoListLayoutComponent,
    CreateSystemDialogComponent,
    ClusterPlotComponent,
    LinePlotComponent],
  entryComponents: [
    CreateSystemDialogComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatDialogModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    PlotlyModule,
  ],
  exports: [
    InsoHeaderComponent,
    InsoDetailLayoutComponent,
    InsoListLayoutComponent,
    ClusterPlotComponent,
    LinePlotComponent
  ],
  providers: [
  ]
})
export class SharedModule {
}
