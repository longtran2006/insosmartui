import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SystemSaveRequestDc} from '../../contracts/systems.contracts';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-system-dialog',
  templateUrl: './create-system-dialog.component.html',
  styleUrls: ['./create-system-dialog.component.css']
})
export class CreateSystemDialogComponent implements OnInit {

  public systemForm: FormGroup;
  public systemTypes: string[] = [
    'Carbotec'
  ];

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateSystemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SystemSaveRequestDc) {
  }

  ngOnInit() {
    this.systemForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      system_type: ['', Validators.required],
    });
  }

  onNoClick() {
    this.dialogRef.close(null);
  }

  onOkClick() {
    /*
    const systemSaveRequestDc: SystemSaveRequestDc = {
      name: this.systemForm.get('name').value,
      description: this.systemForm.get('description').value,
      system_type: this.systemForm.get('system_type').value,
    };
    */
    const systemRequest: SystemSaveRequestDc = this.systemForm.value;

    this.dialogRef.close(systemRequest);
  }

  isInvalid(): boolean {
    return this.systemForm.invalid;
  }
}
