import {Component, Input, OnInit} from '@angular/core';
import {CommandAction} from '../shared.dtos';

@Component({
  selector: 'app-inso-detail-layout',
  templateUrl: './inso-detail-layout.component.html',
  styleUrls: ['./inso-detail-layout.component.css']
})
export class InsoDetailLayoutComponent implements OnInit {
  @Input() detailName: string;
  @Input() actions: CommandAction[] = [];
  @Input() backAction: CommandAction = null;
  abcd = [1, 2, 3, 4];

  get hasBackButton() {
    return this.backAction != null;
  }

  constructor() {
  }

  ngOnInit() {
    console.log(this.actions);
  }

  getColor(level: string) {
    if (level === 'info') {
      return 'primary';
    } else if (level === 'warn') {
      return 'accent';
    } else if (level === 'danger') {
      return 'warn';
    } else {
      return '';
    }
  }

}
