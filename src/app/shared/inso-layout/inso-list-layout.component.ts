import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Column} from '../shared.dtos';

@Component({
  selector: 'app-inso-list-layout',
  templateUrl: './inso-list-layout.component.html',
  styleUrls: ['./inso-list-layout.component.css']
})
export class InsoListLayoutComponent implements OnInit, OnChanges {

  @Input() dataSource: any[] = [];
  @Input() columns: Column[] = [];

  @Output() onPressed = new EventEmitter();

  selectedRowIndex = -1;

  get columnKeys(): string[] {
    return this.columns.map(c => c.key);
  }

  constructor() { }

  ngOnInit() {
  }

  onClicked(data: any) {
    this.selectedRowIndex = data.id;
    this.onPressed.emit(data);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataSource) {
      console.log('Data source changed');
    }
  }
}
