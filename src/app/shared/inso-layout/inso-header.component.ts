import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-inso-header-layout',
  templateUrl: './inso-header.component.html',
  styleUrls: ['./inso-header.component.css']
})
export class InsoHeaderComponent implements OnInit {
  @Input() headerName: string;

  constructor() { }

  ngOnInit() {
  }

}
