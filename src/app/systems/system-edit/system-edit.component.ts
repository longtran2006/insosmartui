import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SystemsService} from '../systems.service';
import {MeasurementIdentityDc, SystemDc} from '../../contracts/systems.contracts';
import {CommandAction} from '../../shared/shared.dtos';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-system-edit',
  templateUrl: './system-edit.component.html',
  styleUrls: ['./system-edit.component.css']
})
export class SystemEditComponent implements OnInit, OnDestroy {
  Object = Object;

  subscription: Subscription;
  system: SystemDc;
  id: number;
  selectedTrainType = 1;

  backAction: CommandAction = {
    actionName: 'Back',
    action: () => this.onGoingBack(),
    level: 'info',
    enabled: true,
    visible: true
  };

  actions: CommandAction[] = [
  ];
  private readonly systemService: SystemsService;
  private intervalId: number;
  firstStepForm: FormGroup;
  secondStepForm: FormGroup;


  constructor(systemService: SystemsService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar) {
    this.systemService = systemService;
  }
  ngOnInit() {
    this.firstStepForm = this.formBuilder.group({
      allowNextStep: [false, Validators.requiredTrue]
    });

    this.secondStepForm = this.formBuilder.group({
      allowNextStep: [false, Validators.requiredTrue]
    });

    this.route.params
      .subscribe(
        (params: Params) => {
          if (!params.id) {return; }
          this.id = +params.id;
          this.systemService.getDetailSystem(this.id).then(sys => {
            this.system = sys;
            console.log(this.system);
            if (this.system != null) {
              this.firstStepForm.patchValue({allowNextStep: this.system.is_imported});
              this.secondStepForm.patchValue({allowNextStep: this.system.is_trained});
            }
          });

        }
      );
    this.subscription = this.systemService.notifications
      .subscribe(
        (msg: any) => {
          console.log('Notification from the msg:', msg);
          if (msg != null && (msg.topic === 'train_carbotec_data_task' || msg.topic === 'import_data_from_csv') ) {
            this.snackBar.open(msg.data.progress, 'close', {
              duration: 3000,
            });

          }

          this.systemService.getDetailSystem(this.id).then(sys => {
            this.system = sys;
            if (this.system != null) {
              this.firstStepForm.patchValue({allowNextStep: this.system.is_imported});
              this.secondStepForm.patchValue({allowNextStep: this.system.is_trained});
            }
          });

        }
      );
  }
  onGoingBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onTrainSystem() {
    const data = {
      train_type: this.selectedTrainType,
    };
    console.log('train system: ', data);
    this.systemService.trainDataSystem(this.id, data);
  }

  onImportDataSystem() {
    this.systemService.importDataSystem(this.id);
  }

  canTrainSystem(): boolean {
    return this.selectedTrainType !== 0 && this.system != null && (this.system.is_trained
      || !this.system.is_trained && this.system.trainned_date != null);
  }
}
