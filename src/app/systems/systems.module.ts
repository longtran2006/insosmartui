import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatDividerModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule, MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';
import {SystemListComponent} from './system-list/system_list.component';
import {SystemsComponent} from './systems.component';
import {SystemDetailComponent} from './system-detail/system-detail.component';
import {SystemEditComponent} from './system-edit/system-edit.component';
import {SystemRoutingModule} from './system.route';
import {SharedModule} from '../shared/shared.module';
import {SystemClusterComponent} from './systems-statistics/system-cluster.component';
import {SystemRegressionComponent} from './systems-statistics/system-regression.component';
import {SystemSomComponent} from './systems-statistics/system-som.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSnackBarModule} from '@angular/material/snack-bar';


@NgModule({
  declarations: [
    SystemListComponent,
    SystemsComponent,
    SystemDetailComponent,
    SystemEditComponent,
    SystemClusterComponent,
    SystemRegressionComponent,
    SystemSomComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,

    SystemRoutingModule,
    SharedModule,

    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatTableModule,
    MatDividerModule,
    MatSidenavModule,
    MatChipsModule,
    MatTabsModule,
    MatSelectModule,
    MatGridListModule,
    MatStepperModule,
    MatSnackBarModule
  ],
  providers: []
})
export class SystemsModule {
}
