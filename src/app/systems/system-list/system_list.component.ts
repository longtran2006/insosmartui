import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {SystemDc} from '../../contracts/systems.contracts';
import {ActivatedRoute, Router} from '@angular/router';
import {SystemsService} from '../systems.service';

@Component({
  selector: 'app-system-list',
  templateUrl: './system_list.component.html',
  styleUrls: ['./system_list.component.css']
})
export class SystemListComponent implements OnInit, OnDestroy {
  systems: SystemDc[] = [];
  subscription: Subscription;
  columns = [
    {key: 'name', name: 'Name'},
    {key: 'description', name: 'Description'},
    {key: 'system_type', name: 'Type'},
  ];

  constructor(private systemService: SystemsService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.subscription = this.systemService.systemChanged
      .subscribe(
        (systemDcs: SystemDc[]) => {
          console.log('System has changed');
          this.systems = [...systemDcs];
        }
      );
    this.systemService.getSystems();

  }

  onNavigate(element: SystemDc) {
    this.router.navigate([element.id], {relativeTo: this.route});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
