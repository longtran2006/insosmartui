import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SystemsComponent} from './systems.component';
import {GuardService} from '../auth/guard.service';
import {SystemDetailComponent} from './system-detail/system-detail.component';
import {SystemEditComponent} from './system-edit/system-edit.component';
import {SystemClusterComponent} from './systems-statistics/system-cluster.component';
import {SystemRegressionComponent} from './systems-statistics/system-regression.component';
import {SystemSomComponent} from './systems-statistics/system-som.component';

const systemRoutes: Routes = [
  { path: '', component: SystemsComponent, children: [
      { path: '', component: SystemDetailComponent },
      { path: 'new', component: SystemEditComponent, canActivate: [GuardService] },
      { path: ':id', component: SystemDetailComponent, canActivate: [GuardService] },
      { path: ':id/edit', component: SystemEditComponent, canActivate: [GuardService] },
      { path: ':id/cluster', component: SystemClusterComponent, canActivate: [GuardService] },
      { path: ':id/regression', component: SystemRegressionComponent, canActivate: [GuardService] },
      { path: ':id/som', component: SystemSomComponent, canActivate: [GuardService] },
    ]},
];

@NgModule({
  imports: [
    RouterModule.forChild(systemRoutes)
  ],
  exports: [RouterModule],
  providers: [
    GuardService
  ]
})
export class SystemRoutingModule {}
