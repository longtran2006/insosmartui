import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SystemSaveRequestDc} from '../contracts/systems.contracts';
import {CreateSystemDialogComponent} from '../shared/dialogs/create-system-dialog.component';
import {MatDialog} from '@angular/material';
import {SystemsService} from './systems.service';

@Component({
  selector: 'app-system',
  templateUrl: './systems.component.html',
  styleUrls: ['./systems.component.css']
})
export class SystemsComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private systemService: SystemsService,
              public dialog: MatDialog) { }

  ngOnInit() {
  }

  onNewSystem() {
    // this.router.navigate(['new'], {relativeTo: this.route});
    const dialogRef = this.dialog.open(CreateSystemDialogComponent, {
      width: '60%',
      height: '60%',
      data: {name: 'Sys1', description: 'Test'} as SystemSaveRequestDc
    });

    dialogRef.afterClosed().subscribe((result: SystemSaveRequestDc) => {
      console.log(result);
      if (result != null) {
        this.systemService.createSystem(result).then(res => {
          console.log(res);
        });
      }
    });
  }


}
