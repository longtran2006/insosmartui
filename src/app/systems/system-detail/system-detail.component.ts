import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SystemsService} from '../systems.service';
import {MeasurementIdentityDc, SystemDc} from '../../contracts/systems.contracts';
import {CommandAction} from '../../shared/shared.dtos';
import {Data, Layout} from 'plotly.js';
import {color} from 'd3';

@Component({
  selector: 'app-system-detail',
  templateUrl: './system-detail.component.html',
  styleUrls: ['./system-detail.component.css']
})
export class SystemDetailComponent implements OnInit, OnDestroy {
  Object = Object;

  system: SystemDc;
  id: number;

  graph = {
    data: [] as Data[],
    layout: null as Layout,
  };

  actions: CommandAction[] = [
    {
      actionName: 'Edit',
      action: () => this.onEditSystem(),
      level: 'warn',
      enabled: true,
      visible: true
    } as CommandAction,
    {
      actionName: 'Delete',
      action: () => this.onDeleteSystem(),
      level: 'danger',
      enabled: true,
      visible: true,
    } as CommandAction,
    /*
    {
      actionName: 'Import data',
      action: () => this.onImportDataSystem(),
      level: 'info',
      enabled: true,
      visible: true
    } as CommandAction,
    {
      actionName: 'Train data',
      action: () => this.onTrainSystem(),
      level: 'info',
      enabled: true,
      visible: true,
    } as CommandAction
     */
  ];
  private firstComp: number;
  private secondComp: number;
  private readonly systemService: SystemsService;
  private intervalId: number;

  selectedPoint: any = null;
  selectedNewLabel = 1;

  constructor(systemService: SystemsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.systemService = systemService;
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          if (!params.id) {return; }
          this.firstComp = 0;
          this.secondComp = 0;
          this.id = +params.id;
          this.systemService.getDetailSystem(this.id).then(sys => this.system = sys);
          console.log(this.system);

        }
      );
  }

  onEditSystem() {
    this.router.navigate(['edit'], {relativeTo: this.route});
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }

  onDeleteSystem() {
    this.systemService.deleteSystem(this.id);
    this.router.navigate(['/systems']);
  }


  ngOnDestroy(): void {
    clearInterval(this.intervalId);
  }


  onShowClusterPlot() {
    this.router.navigate(['cluster'], {relativeTo: this.route});
  }

  onShowRegressionPlot() {
    this.router.navigate(['regression'], {relativeTo: this.route});
  }

  onShowSomPlot() {
    this.router.navigate(['som'], {relativeTo: this.route});
  }
}
