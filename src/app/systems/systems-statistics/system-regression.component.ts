import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SystemsService} from '../systems.service';
import {MeasurementIdentityDc, SystemDc} from '../../contracts/systems.contracts';
import {CommandAction} from '../../shared/shared.dtos';
import {Data, Layout} from 'plotly.js';
import {color} from "d3";

@Component({
  selector: 'app-system-regression',
  templateUrl: './system-regression.component.html',
  styleUrls: ['./system-regression.component.css']
})
export class SystemRegressionComponent implements OnInit, OnDestroy {
  Object = Object;

  system: SystemDc;
  id: number;

  graph = {
    data: [] as Data[],
    layout: null as Layout,
  };
  backAction: CommandAction = {
    actionName: 'Back',
    action: () => this.onGoingBack(),
    level: 'info',
    enabled: true,
    visible: true
  };

  actions: CommandAction[] = [
  ];
  private readonly systemService: SystemsService;
  private intervalId: number;

  constructor(systemService: SystemsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.systemService = systemService;
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          if (!params.id) {return; }
          this.id = +params.id;
          this.systemService.getDetailSystem(this.id).then(sys => this.system = sys);
          console.log(this.system);

          this.systemService.getRegressionData(this.id).then(res => {
            console.log(res);
            const modelType = res.model;
            let ptitle = 'Measurement data with labels';
            if (modelType === 'SomKmeanModel' ) {
              ptitle = 'Measurement data with labels after clustering by SOM and Kmean';
            } else if (modelType === 'IvisKmeanModel') {
              ptitle = 'Measurement data with labels after clustering by IVIS and Kmean';
            } else if (modelType === 'PcaKMeanModel') {
              ptitle = 'Measurement data with labels after clustering by PCA and Kmean';
            }

            this.graph = {
              data: [
                {x: res.time, y: res.furnace, name: 'furnace temperature',
                  type: 'scatter', mode: 'lines+markers', marker: {color: res.cols}},
                {x: res.time, y: res.augerSpeed, name: 'carbon auger speed',
                  type: 'scatter', mode: 'lines+markers', marker: {color: res.cols}},
              ],
              layout: {
                width: 1300, height: 600, title: ptitle,
                xaxis: {
                  title: 'Time',
                },
                yaxis: {
                  title: 'Measurement value',
                }
              } as Layout
            };
          });
        }
      );
  }
  onGoingBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalId);
  }

  onPointSelected(event: any) {
    console.log(event);
    // this.selectedPoint = event;
  }
}
