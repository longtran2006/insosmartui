import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SystemsService} from '../systems.service';
import {MeasurementIdentityDc, SystemDc} from '../../contracts/systems.contracts';
import {CommandAction} from '../../shared/shared.dtos';
import {Data, Layout} from 'plotly.js';
import {color} from 'd3';

@Component({
  selector: 'app-system-cluster',
  templateUrl: './system-cluster.component.html',
  styleUrls: ['./system-cluster.component.css']
})
export class SystemClusterComponent implements OnInit, OnDestroy {
  Object = Object;

  system: SystemDc;
  id: number;

  graph = {
    data: [] as Data[],
    layout: null as Layout,
  };
  backAction: CommandAction = {
    actionName: 'Back',
    action: () => this.onGoingBack(),
    level: 'info',
    enabled: true,
    visible: true
  };


  private firstComp: number;
  private secondComp: number;
  private readonly systemService: SystemsService;
  private intervalId: number;

  selectedPoint: any = null;
  selectedNewLabel = 1;

  constructor(systemService: SystemsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.systemService = systemService;
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          if (!params.id) {return; }
          this.firstComp = 0;
          this.secondComp = 0;
          this.id = +params.id;
          this.systemService.getDetailSystem(this.id).then(sys => this.system = sys);
          console.log(this.system);


          this.systemService.getClusterData(this.id).then(res => {
            console.log(res);

            const modelType = res.model;
            let xTitle = '';
            let yTitle = '';
            let ptitle = 'Clustering plot';
            if (modelType === 'SomKmeanModel' ) {
              ptitle = 'Clustering plot with SOM and KMeans';
              xTitle = 'first SOM output component';
              yTitle = 'second SOM output component';
            } else if (modelType === 'IvisKmeanModel') {
              ptitle = 'Clustering plot with Ivis and KMeans';
              xTitle = 'first IVIS output component';
              yTitle = 'second IVIS output component';
            } else if (modelType === 'PcaKMeanModel') {
              ptitle = 'Clustering plot with Pca and KMeans';
              xTitle = 'first PCA component';
              yTitle = 'second PCA component';
            }
            this.graph = {
              data: [
                {x: res.x, y: res.y, type: 'scatter', mode: 'markers', marker: {color: res.cols}}
              ],
              layout: {
                width: 600, height: 600,
                title: ptitle,
                xaxis: {
                  title: xTitle,
                },
                yaxis: {
                  title: yTitle,
                }
              } as Layout
            };
          });

          this.intervalId = setInterval(() => this.fetchingData(), 5000);
        }
      );
  }

  onEditSystem() {
    this.router.navigate(['edit'], {relativeTo: this.route});
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }

  onGoingBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  onDeleteSystem() {
    this.systemService.deleteSystem(this.id);
    this.router.navigate(['/systems']);
  }

  fetchingData(): TimerHandler {
    return;

    if (this.systemService == null) {
      console.log('Why null?');
      return;
    }

    console.log('Method called');
    this.firstComp += 1;
    this.secondComp += 1;
    this.systemService.getPredictedData(this.id, {first_comp: this.firstComp, second_comp: this.secondComp}).then(
      res => {
        console.log(res);
        // @ts-ignore
        this.graph.data[0].x = this.graph.data[0].x.concat(res.x);
        // @ts-ignore
        this.graph.data[0].y = this.graph.data[0].y.concat(res.y);
        // @ts-ignore
        this.graph.data[0].marker.color = this.graph.data[0].marker.color.concat('rgb(255,255,255)');
        }

    );
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalId);
  }

  onPointSelected(event: any) {
    console.log(event);
    // this.selectedPoint = event;
    if (event && event.color) {
      let index = 0;
      const colorsIdx = {0: 'rgb(0,0,0)', 1: 'rgb(66, 134, 244)', 2: 'rgb(151, 161, 178)',
        3: 'rgb(188, 59, 20)', 4: 'rgb(181, 193, 13)', 5: 'rgb(15, 193, 149)'};
      if (event.color === colorsIdx['1']) {
        index = 1;
      } else if (event.color === colorsIdx['2']) {
        index = 2;
      } else if (event.color === colorsIdx['3']) {
        index = 3;
      } else if (event.color === colorsIdx['4']) {
        index = 4;
      } else if (event.color === colorsIdx['5']) {
        index = 5;
      }
      const data = {
        first_comp: event.x,
        second_comp: event.y,
        label: index
      } as MeasurementIdentityDc;

      this.systemService.getMeasuredData(this.id, data).then(res => {
        console.log(res);
        this.selectedPoint = res;
        this.selectedNewLabel = this.selectedPoint.label;
        console.log(this.selectedNewLabel);
      });
    }
  }

  onUpdateLabel() {

    const data = {
      measurement_id: this.selectedPoint.id,
      new_label: this.selectedNewLabel
    };
    this.systemService.updateMeasuredData(this.id, data).then(res => {
      console.log(res);
    });
  }
}
