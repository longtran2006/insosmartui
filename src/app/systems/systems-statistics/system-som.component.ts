import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SystemsService} from '../systems.service';
import {MeasurementIdentityDc, SystemDc} from '../../contracts/systems.contracts';
import {CommandAction} from '../../shared/shared.dtos';
import {Data, Layout} from 'plotly.js';
import {color} from "d3";

@Component({
  selector: 'app-system-som',
  templateUrl: './system-som.component.html',
  styleUrls: ['./system-som.component.css']
})
export class SystemSomComponent implements OnInit, OnDestroy {
  Object = Object;

  system: SystemDc;
  id: number;

  graph = {
    data: [] as Data[],
    layout: null as Layout,
  };
  backAction: CommandAction = {
    actionName: 'Back',
    action: () => this.onGoingBack(),
    level: 'info',
    enabled: true,
    visible: true
  };

  actions: CommandAction[] = [
  ];
  private firstComp: number;
  private secondComp: number;
  private readonly systemService: SystemsService;
  private intervalId: number;

  selectedPoint: any = null;

  constructor(systemService: SystemsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.systemService = systemService;
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          if (!params.id) {return; }
          this.firstComp = 0;
          this.secondComp = 0;
          this.id = +params.id;
          this.systemService.getDetailSystem(this.id).then(sys => this.system = sys);
          console.log(this.system);

          this.systemService.getSomData(this.id).then(res => {
            console.log(res);
            this.graph = {
              data: [
                {x: res.x, y: res.y, type: 'scatter', mode: 'markers', marker: {color: res.cols}}
              ],
              layout: {width: 700, height: 700, title: 'A SOM Cluster plot'} as Layout
            };
          });

        }
      );
  }

  onGoingBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalId);
  }
}
