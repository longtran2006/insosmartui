import {EventEmitter, Injectable} from '@angular/core';
import {WebsocketService} from '../shared/websocket.service';
import {AuthenticationService} from '../auth/authentication.service';
import {Subject} from 'rxjs';
import {BaseService, RequestDc, Pagination, SocketEndpoint, MessageDc} from '../contracts/base.contracts';
import {map} from 'rxjs/operators';
import {
  ISystemApiService, MeasurementIdentityDc,
  MeasurementPredictingDataDc,
  SystemApiUrl,
  SystemDc,
  SystemSaveRequestDc
} from '../contracts/systems.contracts';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SystemsService extends BaseService implements ISystemApiService {

  public messages: Subject<MessageDc>;
  public systemChanged = new EventEmitter<SystemDc[]>();
  public notifications = new EventEmitter<MessageDc>();

  private page: Pagination;
  private systems: SystemDc[] = [];

  constructor(http: HttpClient,
              private authService: AuthenticationService,
              private wsService: WebsocketService) {
    super(http);
    if (authService.isAuthenticated) {
      this.messages = this.wsService.connect(SocketEndpoint + '/systems/').pipe(map(
        (response: MessageEvent): MessageDc => {
          const data = JSON.parse(response.data);
          return data;
        }
      )) as Subject<RequestDc>;

      this.messages.subscribe(msg => {
        console.log('Response from web socket: ');
        console.log(msg);
        this.onMessageReceived(msg);
      });
    }
  }

  public sendMessage(msg: RequestDc) {
    this.messages.next(msg);
  }

  createSystem(request: SystemSaveRequestDc): Promise<SystemDc> {
    return new Promise((resolve, reject) => {
      this.doPost(SystemApiUrl.createSystem, request).toPromise()
        .then((res: SystemDc) => {
          this.systems.push(res);
          this.systemChanged.emit(this.systems);
          resolve(res);
        })
        .catch(err => reject(err));
    });
  }

  deleteSystem(id: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.doDelete(SystemApiUrl.deleteSystem + id).toPromise()
        .then(res => {
          const sysIndex = this.systems.findIndex(s => s.id === id);
          if (sysIndex !== -1) {
            this.systems.splice(sysIndex, 1);
            this.systemChanged.emit(this.systems);
            resolve(true);
          }
        })
        .catch(err => reject(err));
    });
  }

  getAllSystems(page: Pagination): SystemDc[] {
    this.page = page;
    return this.doGetWithPagination(SystemApiUrl.getAllSystems, page)
      .subscribe(
        (data: SystemDc[]) => {
          console.log(data);
          this.systems = data;
          this.systemChanged.emit(this.systems);
        }
      );
  }

  getDetailSystem(id: number): Promise<SystemDc> {
    return new Promise((resolve, reject) => {
      this.doGet(SystemApiUrl.detailSystem + id).toPromise()
        .then(
          (system: SystemDc) => {
            const sysIndex = this.systems.findIndex(s => s.id === system.id);
            if (sysIndex !== -1) {
              this.systems[sysIndex] = system;
            } else {
              this.systems.push(system);
            }
            // console.log('Get data ', system);
            // this.systemChanged.emit(this.systems);
            resolve(system);
          })
        .catch((err) => {
          reject(err);
        });
    });
  }

  getSystems(): SystemDc[] {
    return this.doGet(SystemApiUrl.getAllSystems)
      .subscribe(
        (data: SystemDc[]) => {
          console.log(data);
          this.systems = data;
          this.systemChanged.emit(this.systems);
        }
      );
  }

  updateSystem(id: number, request: SystemSaveRequestDc): any {
    return this.doDelete(SystemApiUrl.deleteSystem + id).toPromise()
      .then((res: SystemDc) => {
        const sysIndex = this.systems.findIndex(s => s.id === id);
        if (sysIndex !== -1) {
          this.systems[sysIndex] = res;
          this.systemChanged.emit(this.systems);
        }
      });
  }

  protected onMessageReceived(msg: RequestDc) {
    // do something here
    this.notifications.emit(msg);
  }

  importDataSystem(id: number): Promise<any> {
    return this.doPost(SystemApiUrl.importDataSystem + id + '/', {}).toPromise();
  }

  trainDataSystem(id: number, data: any): Promise<any> {
    return this.doPost(SystemApiUrl.trainDataSystem + id + '/', data).toPromise();
  }

  getClusterData(id: number): Promise<any> {
    return this.doGet(SystemApiUrl.getClusterData + id).toPromise();
  }

  getPredictedData(id: number, data: MeasurementPredictingDataDc): Promise<any> {
    return this.doPost(SystemApiUrl.getPredictedData + id + '/', data).toPromise();
  }

  getMeasuredData(id: number, data: MeasurementIdentityDc): Promise<any> {
    return this.doPost(SystemApiUrl.getMeasuredData + id + '/', data).toPromise();
  }

  updateMeasuredData(id: number, data: any): Promise<any> {
    return this.doPost(SystemApiUrl.updateMeasuredData + id + '/', data).toPromise();
  }

  getRegressionData(id: number): Promise<any> {
    return this.doGet(SystemApiUrl.getRegressionData + id).toPromise();
  }

  getSomData(id: number): Promise<any> {
    return this.doGet(SystemApiUrl.getSomClusterData + id).toPromise();
  }
}
