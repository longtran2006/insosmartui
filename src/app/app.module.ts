import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {AppRouterModule} from './app.route';
import {AuthModule} from './auth/auth.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from './auth/token.interceptor';
import {SystemsModule} from './systems/systems.module';
import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material';

import * as plotlyJS from 'plotly.js/dist/plotly.js';
import {PlotlyModule} from 'angular-plotly.js';
import {WebsocketService} from './shared/websocket.service';

PlotlyModule.plotlyjs = plotlyJS;
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,

    // Router
    AppRouterModule,

    // Insosmart owned modules:
    SharedModule,
    AuthModule,
    CoreModule,
    // Angular materials
    BrowserAnimationsModule,
    PlotlyModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    {provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        disableClose: true,
        width: '60%',
        maxHeight: '60%'
      }},
    WebsocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
